#pragma once
#include "Point.h"

#include <string>
#include <vector>

class ConfigParse
{
public:

	std::string configText;

	ConfigParse(std::string args);

	void filter();
	void removeComments();
	void removeLeadingSpace();
	void removeEmptyLines();

	std::string nextVariable();
	std::vector<Point> nextPointArray();
	std::string nextString();
	int nextInt();
	bool nextBool();

	int nextIndex(unsigned int start, char chr);
	std::string substring(int start, int end);
	void cut(int start, int end);
	std::string toString();
	~ConfigParse();
private:
	ConfigParse();
};

