#pragma once

#include "Point.h"

class System
{
public:
	static Point getScreenResolution();
private:
	System();
	~System();
};

