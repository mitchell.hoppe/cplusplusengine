#include "Point.h"

#include <string>
#include <algorithm>

int x_coor;
int y_coor;


/*
 * Default constructor for the Point class, takes in x,y ints as parameters.
 */
Point::Point(int x, int y)
{
	this->x_coor = x;
	this->y_coor = y;
}

/*
 * Constructor for Point, takes in string representation of a Point object and parses it
 */
Point::Point(std::string param)
{
	std::string point = param;
	point.erase(std::remove(point.begin(), point.end(), ' '), point.end());
	this->x_coor = std::stoi(point.substr(1,point.find(',')));
	this->y_coor = std::stoi(point.substr(point.find(',') + 1, point.length() - 1 - (point.find(',') + 1)));
}

/*
 * Returns the x coordinate
 */
int Point::getX()
{
	return x_coor;
}

/*
 * Returns the y coordinate
 */
int Point::getY()
{
	return y_coor;
}

/* 
 * Returns a string representation of the Point object
 */
std::string Point::toString()
{
	return "[ " + std::to_string(x_coor) + " , " + std::to_string(y_coor) + " ]";
}

/*
 * Returns true if the two points are equal
 */
bool Point::equals(Point p)
{
	return x_coor == p.getX() && y_coor == p.getY();
}

Point::~Point(){}
