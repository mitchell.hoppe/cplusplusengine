#include "ConfigParse.h"
#include "Point.h"

#include <string>
#include <vector>
#include <algorithm>

/*
 * ConfigParse.cpp 
 *
 * This class is intended to take a configuration file in the cppengine project and 
 * return usable variable types. This class is to be used VERY SPECIFICALLY. 
 * Any deviation from the intended use of this class will lead to bad parsing,  
 * wrong values, and errors thrown.
 *
 * How to use the ConfigParse.cpp class:
 * 1. The constructor for the class will take the ENTIRE text of the configuration
 *    file as its argument. The constructor will then take the text and filter out
 *    all the comments, tabs, and other unneeded charaters.
 * 2. The calling class MUST get the name of the variable BEFORE the value
 *    associated with it. And the class will only return the names and values of 
 *    variables in the order that they appear in the file. 
 * 3. Finally calls to get the value of the varaible will come after the call to
 *    get the name. As well as, the CORRECT function must be called to match the 
 *    value-type of the variable in the config file. If a mis-match occurs, then
 *    the parser will be confused as to what its parsing. 
 *    
 * This class currently supports the following variable types:
 *    a. Array/Vector of Points (engine/utils/Point.h)
 *    b. string
 *    c. integer
 *    d. bool
 * 
 * TODO: 
 * 
 * Add support for the following varaible types:
 *    x. char
 *    y. float
 *    z. double
 * 
 * Complete overhaul of the class:
 *    The class as it stands now is not the most efficient implementation of config
 *     file parsing. While it works for my purposes, it's not bug free and there is 
 *     probably redundant loops within the class. Until I think of a better structure
 *     and have the opportunity to implement it. The class will stay as is.
 * 
 */

std::string configText;

/*
 * The main constructor for the class. 
 * Takes the entire text of the config file as its argument
 */
ConfigParse::ConfigParse(std::string args)
{
	this->configText = args;
	filter();
}

/*
 * This function removes all comments, empty lines, and tabs from the 
 * text of the global varaible
 */
void ConfigParse::filter()
{
	configText.erase(std::remove(configText.begin(), configText.end(), '\t'), configText.end());
	removeComments();
	removeEmptyLines();
	removeLeadingSpace();
}

/*
 * Removes all the comments by removing content inbetween '#'s and 
 * the next new line charater
 */
void ConfigParse::removeComments()
{
	int poundIndex = configText.find('#');
	while (poundIndex != -1)
	{
		int newlineIndex = nextIndex(poundIndex, '\n') + 1;
		cut(poundIndex, newlineIndex);
		poundIndex = configText.find('#');
	}
}

/*
 * Removes any unnessisary content at the beginning of the string
 */
void ConfigParse::removeLeadingSpace()
{
	unsigned int i;
	for (i = 0; i < configText.length() && (configText.at(i) == ' '  ||
											configText.at(i) == '='  ||
											configText.at(i) == '\"' ||
											configText.at(i) == '\n')	; i++)
	{}
	cut(0, i);
}

/*
 * Removes lines whose content is just spaces, or non-existant
 */
void ConfigParse::removeEmptyLines()
{
	for (unsigned int i = 0; i < configText.length() - 1; i++)
	{
		if (configText.at(i) == '\n' && configText.at(i + 1) == '\n')
		{
			configText =	configText.substr(0, i) +
							configText.substr(i + 1);
			i--;
		}
		else if (configText.at(i) == '\n' || (i == 0 && configText.at(i) == ' '))
		{
			unsigned int spaceIndex = i + 1;
			while (configText.at(spaceIndex) == ' ' && 
				spaceIndex < configText.length() - 1)
			{
				spaceIndex++;
			}
			int length = configText.length();
			if (configText.at(spaceIndex) == '\n' || spaceIndex == configText.length() - 1)
			{
				configText =	configText.substr(0, i) +
								configText.substr(spaceIndex + 1);
				i--;
			}
		}
	}
}

/* 
 * Returns the next variable name as a string
 */
std::string ConfigParse::nextVariable()
{
	std::string varName = "";

	for (unsigned int i = 0; i < configText.length() && (configText.at(i) == '_' || isalpha(configText.at(i))); i++)
	{
		varName += configText.at(i);
		cut(i, i + 1);
		i--;
	}

	removeLeadingSpace();
	return varName;
}

/* 
 * Returns the vector of Points
 */
std::vector<Point> ConfigParse::nextPointArray()
{
	std::vector<Point> points = std::vector<Point>();

	unsigned int index = 0;
	unsigned int lastIndex = index + 1;
	unsigned int stackDepth = (configText.at(index) == '[' ? 1 : 0);

	while (index < configText.length() && stackDepth != 0)
	{
		index++;
		if (configText.at(index) == '[')
		{
			stackDepth++;
			lastIndex = index;
		}
		else if (configText.at(index) == ']')
		{
			stackDepth--;
		}
		
		if (configText.at(index) == ',' && stackDepth == 1)
		{
			std::string pointString = substring(lastIndex, index - 1);

			Point point = Point::Point(pointString);
			points.push_back(point);
		}
	}
	index++;

	std::string pointString = substring(lastIndex, index - 2);
	cut(0, index);
	Point point = Point::Point(pointString);
	points.push_back(point);

	removeLeadingSpace();
	return points;
}

/*
 * Returns the text inbetween the quotations as a string
 */
std::string ConfigParse::nextString()
{
	removeLeadingSpace();

	std::string value = "";

	unsigned int index = 0;
	unsigned int lastIndex = index;

	while (index < configText.length() && configText.at(index) != '\"')
	{
		index++;
	}
	value = substring(lastIndex, index - 1);
	cut(0, index);

	removeLeadingSpace();
	return value;
}

/*
 * Returns the next integer
 */
int ConfigParse::nextInt()
{
	unsigned int index = 0;

	while (index < configText.length() && isdigit(configText.at(index)) )
	{
		index++;
	}
	int value = std::stoi( substring(0, index - 1) );
	cut(0, index);

	removeLeadingSpace();
	return value;
}

/*
 * Returns the next boolean if there is a parsing error, return NULL
 */
bool ConfigParse::nextBool()
{
	if (configText.length() >= 4)
	{
		std::string toLower = configText.substr(0, 4);
		std::transform(toLower.begin(), toLower.end(), toLower.begin(), ::tolower);
		if (toLower == "true")
		{
			cut(0,4);
			removeLeadingSpace();
			return true;
		}
	}
	if (configText.length() >= 5)
	{
		std::string toLower = configText.substr(0, 5);
		std::transform(toLower.begin(), toLower.end(), toLower.begin(), ::tolower);
		if (toLower == "false")
		{
			cut(0, 5);
			removeLeadingSpace();
			return false;
		}
	}
	return NULL;
}

/*
 * Returns the substring in-between the given indexes
 */
std::string ConfigParse::substring(int start, int end)
{
	return configText.substr(start , end - start + 1);
}

/*
 * Removes the content in the config text in-between the
 * given indexes
 */
void ConfigParse::cut(int start, int end)
{
	configText =	configText.substr(0, start) +
					configText.substr(end);
}

/*
 * Returns the index of the next occurance of the given charater after 
 * the given starting index
 */
int ConfigParse::nextIndex(unsigned int start, char chr)
{
	if (start > configText.length())
		return -1;
	for (unsigned int i = start; i < configText.length(); i++)
	{
		char at = configText.at(i);
		if (at == chr)
			return i;
	}
	return -1;
}

/*
 * Returns the config text
 */
std::string ConfigParse::toString()
{
	return configText;
}

ConfigParse::~ConfigParse(){}
ConfigParse::ConfigParse() {}
