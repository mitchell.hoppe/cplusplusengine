#include "Utils.h"

#include <string>
#include <windows.h>
#include <vector>

std::string Utils::getProjectDirectory()
{
	char result[MAX_PATH];
	std::string path = std::string(result, GetModuleFileName(NULL, result, MAX_PATH));
	path = path.substr(0, path.find_last_of("/\\"));
	path = path.substr(0, path.find_last_of("/\\"));
	return path;
}

int Utils::randomNum(int start, int end)
{
	return rand() % (end - start + 1) + start;
}

Utils::Utils(){}
Utils::~Utils(){}
