#pragma once

#include <string>

class Utils
{

public:
	static std::string getProjectDirectory();
	static int randomNum(int start, int end);
private:
	Utils();
	~Utils();
};

