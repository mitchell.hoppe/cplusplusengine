#pragma once

#include <string>

class Point
{
public:
	Point(int x, int y);
	Point(std::string point);
	int getX();
	int getY();
	std::string toString();
	bool equals(Point p);
	~Point();
private:
	int x_coor;
	int y_coor;
};

