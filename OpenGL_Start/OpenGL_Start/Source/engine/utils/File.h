#pragma once

#include <string>

class File
{

public:
	static std::string readFile(std::string fileName);
	static bool appendFile(std::string fileName, std::string content);
	static bool validateFile(std::string fileName);
	static bool createFile(std::string fullPath);
	static bool deleteFile(std::string fileName);
private:
	File();

	static std::string findFile(std::string fileName);

	~File();
};

