#include "File.h"

#include <string>
#include <fstream>

/*
 * Returns the contents of the fileName as a string
 */
std::string File::readFile(std::string fileName)
{
	std::ifstream ifs(fileName);
	std::string content((std::istreambuf_iterator<char>(ifs)),
		(std::istreambuf_iterator<char>()));
	return content;
}

/*
 * Writes the given string to a new line in the file
 */
bool File::appendFile(std::string fileName, std::string content)
{
	if (!File::validateFile(fileName))
		return false;
	std::ofstream myfile;
	myfile.open(fileName);
	myfile << content << "\n";
	myfile.close();
	return true;
}

/*
 * Returns true if the file exists
 */
bool File::validateFile(std::string fileName)
{
	struct stat buffer;
	return (stat(fileName.c_str(), &buffer) == 0);
}

/*
 * Creates an empty file at the specified file path
 * If the file already exists, return false
 * Precondition: Full file path is valid.
 */
bool File::createFile(std::string fullPath)
{
	if (validateFile(fullPath)) return false;
	std::fstream file(fullPath, std::ios::out | std::ios::app);
	std::string data("");
	file << data;
	return validateFile(fullPath);
}

/*
 * Deletes the specified file, returns the success of the operation
 */
bool File::deleteFile(std::string fileName)
{
	if (remove(fileName.c_str()) != 0)
		return false;
	return true;
}

/**
 * Recursivly searches the project directory for the file specified
 * Not implemented, future feature to be added. Private.
 * TODO: Finish
 */
std::string File::findFile(std::string fileName)
{
	return "";
}

File::File(){}
File::~File(){}
