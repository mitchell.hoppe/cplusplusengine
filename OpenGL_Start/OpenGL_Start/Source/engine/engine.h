#pragma once

#include "io/input/Keyboard.h"

#include <GLFW/glfw3.h>
#include <string>

class Engine
{
public:
	Engine();
	Engine(int mode);
	int init();
	int run();
	void update();
	void render();
	void initConfig();
	void close();
	~Engine();

private:
	int initDEFAULT_VARAIBLES();
	int initCONFIGURED_VARAIBLES();
	int initGLOBAL_VARAIBLES();

	int mode;

	int window_width;
	int window_height;
	std::string window_title;

	GLFWwindow* window;
};

