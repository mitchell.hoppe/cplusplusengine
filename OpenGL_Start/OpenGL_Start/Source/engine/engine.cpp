#include <GLEW/glew.h>
#include "Engine.h"
#include "utils/ConfigParse.h"
#include "utils/File.h"
#include "utils/Utils.h"
#include "io/input/Keyboard.h"
#include "graphics/Render.h"

#include <iostream>
#include <GLFW/glfw3.h>
#include <string>

const std::string ENGINE_CONFIG_PATH = "\\OpenGL_Start\\bin\\config\\engine.conf";
/***************************/
/* DEFAULT VARAIBLES BLOCK */
/***************************/

std::vector<Point> DEFAULT_RESOLUTIONS		= std::vector<Point>();
int DEFAULT_RESOLUTION_INDEX				= 0;
std::string DEFAULT_WINDOW_TITLE			= "cpp engine";

/******************************/
/* CONFIGURED VARAIBLES BLOCK */
/******************************/

std::vector<Point> RESOLUTIONS;
int RESOLUTION_INDEX;
std::string WINDOW_TITLE;

/**************************/
/* GLOBAL VARAIBLES BLOCK */
/**************************/

/* For future use 'mode' determines what mode the engine will run, depending on the args that are given at run time
 * For now, mode does nothing, but will be used in the future when the terminal is implemented
 */
int mode = 0;

int window_width;
int window_height;
std::string window_title;



GLFWwindow* window;

Keyboard g_keyboard;
Render g_render;

/**
 * Default constuctor for the engine class.
 * This class requires that you manually initalize and run the class.
 */
Engine::Engine()
{
	this->mode = 0;
}

/**
 * Constuctor for the engine class.
 * This class requires that you manually initalize and run the class.
 */
Engine::Engine(int mode)
{	
	this->mode = mode;
}

int Engine::initDEFAULT_VARAIBLES()
{
	DEFAULT_RESOLUTIONS.push_back(Point(1920, 1080));

	return 0;
}

int Engine::initCONFIGURED_VARAIBLES()
{
	RESOLUTIONS = DEFAULT_RESOLUTIONS;
	RESOLUTION_INDEX = DEFAULT_RESOLUTION_INDEX;
	WINDOW_TITLE = DEFAULT_WINDOW_TITLE;

	initConfig();

	return 0;
}

int Engine::initGLOBAL_VARAIBLES() 
{
	this->window_width = RESOLUTIONS.at(RESOLUTION_INDEX).getX();
	this->window_height = RESOLUTIONS.at(RESOLUTION_INDEX).getY();
	this->window_title = WINDOW_TITLE;

	return 0;
}

/**
 * Initalizes the library, window, and the context
 * return: int | the return status of the function, will return -1 if there is an error
 */
int Engine::init()
{
	initDEFAULT_VARAIBLES();

	initCONFIGURED_VARAIBLES();

	initGLOBAL_VARAIBLES();

	/* Initialize the library */
	if (!glfwInit())
		return -1;
	/* Set the window to be unresizeable */
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(window_width, window_height, window_title.c_str(), NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}
	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	/*GLEW TEST CODE; DELETE*/
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		/* Problem: glewInit failed, something is seriously wrong. */
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
	}

	g_keyboard = Keyboard::Keyboard(window);

	return 0;
}

/**
 * Runs the window
 * return: int | the return status of the function, will return -1 if there is an error
 */
int Engine::run()
{
	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window))
	{
		/* Render here */
		glClear(GL_COLOR_BUFFER_BIT);

		/* Swap front and back buffers */
		glfwSwapBuffers(window);

		/* Poll for and process events */
		glfwPollEvents();

		update();

		render();
	}
	return 0;
}

void Engine::update() 
{
	g_keyboard.update();	
}

void Engine::render() 
{

}

void Engine::initConfig()
{
	std::string configText = File::readFile(Utils::getProjectDirectory() + ENGINE_CONFIG_PATH);
	ConfigParse configParse = ConfigParse::ConfigParse(configText);

	while(configParse.toString().length() != 0)
	{ 
		std::string name = configParse.nextVariable();
		if(name == "RESOLUTIONS")
		{
			RESOLUTIONS = configParse.nextPointArray();
		} else 
		if(name == "RESOLUTION_INDEX")
		{
			RESOLUTION_INDEX = configParse.nextInt();
		} else 
		if (name == "WINDOW_TITLE")
		{
			WINDOW_TITLE = configParse.nextString();
		}
	}
}

void Engine::close()
{
	glfwSetWindowShouldClose(window, GLFW_TRUE);
	glfwTerminate();
}


/**
 * Cleans up the class by terminating glfw
 */
Engine::~Engine(){}
