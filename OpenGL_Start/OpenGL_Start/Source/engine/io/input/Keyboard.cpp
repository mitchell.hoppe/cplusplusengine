#include "keyboard.h"

#include <iostream>
#include <map>

/*
 * Special thanks to CroCo on stackexchange for the key_callback workaround in glfw.
 * https://gamedev.stackexchange.com/users/39905/croco
 *
 * This is the Keyboard class for the engine. All keyboard keyboard stokes will be routed 
 * through this class to be stored in integer vectors to be pulled during the update cycle.
 */

Keyboard* Keyboard::s_pInstance = NULL;

/* Temporary storage for the keys pressed from the key_callback function */
std::vector<std::pair<int, int>> g_;
/* 
 * initalizes all the global variables in the class, sets the keyCallBack
 */
void Keyboard::init(GLFWwindow* window)
{
	m_pressed = std::vector<int>();
	m_released = std::vector<int>();
	m_repeated = std::vector<int>();
	m_typed = std::vector<int>();

	m_pWindow = window;
	glfwSetKeyCallback(window, key_callback);
}

/*
 * key_callback function passed to glfw for key events
 */
void Keyboard::key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	g_.push_back(std::make_pair(key, action));
}


/*
 * updates the key vectors and clears the temporary key vector.
 */
void Keyboard::update()
{
	update(g_);

	g_.clear();
}

/*
 * updates all of the key vectors in the class from the temporary vector from the 
 * key_callback function
 */
void Keyboard::update(std::vector<std::pair<int, int>> keys)
{

	/* 
	 * Clear the released and typed vectors because the vectors are only 
	 * relevent for one update cycle 
	 */
	m_released.clear();
	m_typed.clear();


	for (unsigned int i = 0; i < keys.size(); i++)
	{
		int k = keys.at(i).first;
		int a = keys.at(i).second;
		
		/* filter out any keys that are not relevent */
		if (k != 0 && k != -1)
		{
			if (a == GLFW_PRESS)
			{
				/* when pressing a key add to typed and pressed */
				m_pressed.push_back(k);
				m_typed.push_back(k);
			} else 
			if (a == GLFW_RELEASE)
			{
				/* 
				 * when a key is released, remove the key from repeated and pressed
				 * also, add the key to m_released
				 */
				for (unsigned int j = 0; j < m_pressed.size(); j++)
				{
					if (m_pressed[j] == k)
					{
						m_pressed.erase(m_pressed.begin() + j);
					}
				}
				for (unsigned int j = 0; j < m_repeated.size(); j++)
				{
					if (m_repeated[j] == k)
					{
						m_repeated.erase(m_repeated.begin() + j);
					}
				}
				m_released.push_back(k);
			} else 
			if (a == GLFW_REPEAT)
			{
				/* 
				 * when a key is repeated, only add to the vector if it is not already
				 * in there
				 */
				bool found = false;
				for (unsigned int j = 0; j < m_repeated.size(); j++)
					if (m_repeated[j] == k)
						found = true;

				if(!found)
					m_repeated.push_back(k);
			}

		}
	}
}

/*
 * returns boolean if the given key is pressed
 */
bool Keyboard::isPressed(int key)
{
	for (unsigned int i = 0; i < m_pressed.size(); i++)
	{
		if (m_pressed.at(i) == key)
			return true;
	}
	return false;
}

/*
 * returns boolean if the given key is released
 */
bool Keyboard::isReleased(int key)
{
	for (unsigned int i = 0; i < m_released.size(); i++)
	{
		if (m_released.at(i) == key)
			return true;
	}
	return false;
}

/*
 * returns boolean if the given key is repeated
 */
bool Keyboard::isRepeated(int key)
{
	for (unsigned int i = 0; i < m_repeated.size(); i++)
	{
		if (m_repeated.at(i) == key)
			return true;
	}
	return false;
}


/*
 * returns boolean if the given key was typed
 */
bool Keyboard::isTyped(int key)
{
	for (unsigned int i = 0; i < m_typed.size(); i++)
	{
		if (m_typed.at(i) == key)
			return true;
	}
	return false;
}

/*
 * returns the vector of pressed keys
 */
std::vector<int> Keyboard::getPressedKeys()
{
	return m_pressed;
}

/*
 * returns the vector of released keys
 */
std::vector<int> Keyboard::getReleasedKeys()
{
	return m_released;
}

/*
 * returns the vector of repeated keys
 */
std::vector<int> Keyboard::getRepeatedKeys()
{
	return m_repeated;
}

/*
 * returns the vector of typed keys
 */
std::vector<int> Keyboard::getTypedKeys()
{
	return m_typed;
}

/*
 * Constructor for the class
 */
Keyboard::Keyboard(GLFWwindow* window)
{
	init(window);
}

Keyboard::Keyboard(){}