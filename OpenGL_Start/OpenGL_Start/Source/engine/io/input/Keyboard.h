#ifndef _KEYBOARD_H
#define _KEYBOARD_H

#include <GLFW/glfw3.h>
#include <vector>
#include <utility>

/*
 * Header file for the Keybaord class
 */
class Keyboard
{
public:
	static Keyboard* Instance() {
		if (s_pInstance == NULL)
			s_pInstance = new Keyboard;
		return s_pInstance;
	}
	void init(GLFWwindow* window);
	void update();

	Keyboard(GLFWwindow* window);
	Keyboard();


	bool isPressed(int key);
	bool isReleased(int key);
	bool isRepeated(int key);
	bool isTyped(int key);

	std::vector<int> getPressedKeys();
	std::vector<int> getReleasedKeys();
	std::vector<int> getRepeatedKeys();
	std::vector<int> getTypedKeys();

private:
	static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);

	GLFWwindow* m_pWindow;
	static Keyboard* s_pInstance;

	std::vector<int> m_pressed;
	std::vector<int> m_released;
	std::vector<int> m_repeated;
	std::vector<int> m_typed;

	void update(std::vector<std::pair<int, int>> keys);

};

#endif