#include "Render.h"

#include <iostream>
#include <GLFW/glfw3.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include "../External/freetype-gl/freetype-gl.h"

/*
 * sets the background color of the window
 * input : the color as 3 integers between 0 and 255
 */
void Render::setBackgroundColor(int r, int g, int b, int a)
{
	glClearColor((float)(r / 255), (float)(g / 255), (float)(b / 255), (float)(a / 255));
}


Render::Render()
{

}

Render::~Render(){}
