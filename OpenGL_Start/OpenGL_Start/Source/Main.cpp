#include "../Source/engine/Engine.h"

#include <iostream>
#include <GLFW/glfw3.h>

#include "../Test/engine/utils/FileTest.h"
#include "../Test/engine/utils/PointTest.h"
#include "../Test/engine/utils/ConfigParseTest.h"
#include "../Test/Playground.h"

/*
 * Since there can only be one main function within a project, this varaible will tell the function what to run
 * 0 : Normal mode, run the engine.
 * 1 : Debug mode, run the tester functions
 */
int MAIN_MODE = 0;
int testerMain();

/**
 * The main function for the engine. Starts and runs the engine.
 */
int main(void)
{
	/* If the Main class is in debug mode, run the nessisary tester functions */
	if (MAIN_MODE == 1) return testerMain();

	Engine engine = Engine::Engine();

	/* initalize the engine. if there was an error the engine will return -1, in that case, stop */
	int initstatus = engine.init();
	if (initstatus != 0)
	{
		std::cout << "[main.cpp] the engine could not be initalized, exiting" << std::endl;
		return -1;
	}

	/* run the engine. if there was an error the engine will return -1, in that case, stop */
	int runstatus = engine.run();
	if (runstatus != 0)
	{
		std::cout << "[main.cpp] there was an issue running the engine, exiting" << std::endl;
		return -1;
	}

	return 0;
}

int testerMain()
{
	//FileTest fileTest = FileTest::FileTest();
	//PointTest pointTest = PointTest::PointTest();
	ConfigParseTest configParseTest = ConfigParseTest::ConfigParseTest();
	//Playground::main();
	return 0;
}