#include "Playground.h"

#include <iostream>
#include <string>

void Playground::main() {
	std::string s = "Test string";
	std::cout << s << std::endl;
	Playground::modString(s);
	std::cout << s << std::endl;
}

void Playground::modString(std::string arg) {
	arg = arg + " mod";
}

Playground::Playground(){}
Playground::~Playground(){}
