#pragma once

#include <string>

class Playground
{
public:
	static void main();
	static void modString(std::string arg);
private:
	Playground();
	~Playground();
};

