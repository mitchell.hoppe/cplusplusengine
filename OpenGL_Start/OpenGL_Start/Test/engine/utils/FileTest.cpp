#include "FileTest.h"
#include "../../../Source/engine/utils/File.h"
#include "../../../Source/engine/utils/Utils.h"

#include <iostream>

/*
 * Testing class for File.cpp
 * This class works not off returning booleans, but by simply running all the tests in house
 * and printing the results to the console
 */

/* Relative path from the Project Directory to a test folder where files can be created and deleted */
const std::string TESTDIR_LOCAL_PATH = "\\OpenGL_Start\\bin\\Test\\";

/*
 * 
 */
FileTest::FileTest()
{
	init();
	run();
}

void FileTest::init()
{
	std::cout << "[FileTest.cpp] Initalizing File.cpp test..." << std::endl;
}

void FileTest::run() 
{
	std::string projectDir = Utils::getProjectDirectory() + TESTDIR_LOCAL_PATH;

	std::cout << "[FileTest.cpp] Testing File.cpp in " << projectDir << " ..." << std::endl;
	for(int i = 0; i < 15; i++)
	{
		std::string fileName = "file_" + std::to_string(i) + ".txt";
		std::string content = "Test file " + fileName + "\nThis file tests the File.cpp class.";
		std::string fullFilePath = projectDir + fileName;

		bool success = true;
		success = success && File::createFile(fullFilePath);
		success = success && File::appendFile(fullFilePath, content);
		success = success && std::strcmp(File::readFile(fullFilePath).c_str(), content.c_str());
		success = success && File::deleteFile(fullFilePath);

		std::cout << "[FileTest.cpp]    " << 
			(success ? "O " + fullFilePath + " created, written, and deleted" : "X " + fullFilePath + " failed") << std::endl;
	}

}


FileTest::~FileTest(){}