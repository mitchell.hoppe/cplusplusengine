#include "ConfigParseTest.h"
#include "../../../Source/engine/utils/ConfigParse.h"
#include "../../../Source/engine/utils/File.h"
#include "../../../Source/engine/utils/Utils.h"

#include <iostream>

const std::string ENGINE_CONFIG_PATH = "\\OpenGL_Start\\bin\\config\\engine.conf";

/*
 * TODO: Insert Comments
 */
ConfigParseTest::ConfigParseTest()
{
	std::cout << "[ConfigParseTest.cpp] Starting ConfigParse.cpp test..." << std::endl;
	init();
	run();
	std::cout << "[ConfigParseTest.cpp] Done." << std::endl;
}

void ConfigParseTest::init()
{
	
}

void ConfigParseTest::run()
{
	std::string configText = File::readFile(Utils::getProjectDirectory() + ENGINE_CONFIG_PATH);
	ConfigParse configParse = ConfigParse::ConfigParse(configText);

	std::cout << configParse.toString() << std::endl;

	std::string name = configParse.nextVariable();

	std::vector<Point> RESOLUTIONS = configParse.nextPointArray();
	for(Point temp : RESOLUTIONS)
		std::cout << "[ConfigParseTest.cpp] " << temp.toString() << std::endl;

	name = configParse.nextVariable();
	name = configParse.nextString();
	std::cout << "[ConfigParseTest.cpp] \"" << name << "\"" << std::endl;

	/*
	name = configParse.nextVariable();
	bool boolean = configParse.nextBool();
	std::cout << "[ConfigParseTest.cpp] " << name << std::endl;
	std::cout << "[ConfigParseTest.cpp] [" << (boolean == NULL ? "null" : (boolean ? "true" : "false")) << "]" << std::endl;
	*/
}

ConfigParseTest::~ConfigParseTest()
{
}
