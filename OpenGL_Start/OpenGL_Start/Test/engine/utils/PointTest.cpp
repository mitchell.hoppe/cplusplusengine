#include "PointTest.h"
#include "../../../Source/engine/utils/Point.h"

#include <algorithm>
#include <iostream>


PointTest::PointTest()
{
	init();
	run();
}

void PointTest::init()
{
	std::cout << "[PointTest.cpp] Initalizing Point.cpp test..." << std::endl;
}

void PointTest::run()
{
	Point p = Point::Point(554,47652);
	std::string toString = p.toString();
	Point p2 = Point::Point(toString);
	
	std::cout << toString << "," << p2.toString() << std::endl;
}



PointTest::~PointTest(){}
